import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import { fetchForecast } from './forecastActions';
import moxios from 'moxios';

const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)

describe('async actions', () => {
    beforeEach(() => {
        moxios.install();
    });

    afterEach(() => {
        moxios.uninstall();
    });

    it('Fetch works', () => {
        const expectedState = {
            "city":{
                "id":1851632,
                "name":"Shuzenji",
                "coord":{
                    "lon":138.933334,
                    "lat":34.966671
                },
                "country":"JP",
                "timezone": 32400,
                "cod":"200",
                "message":0.0045,
                "cnt":38,
                "list":[{
                    "dt":1406106000,
                    "main":{
                        "temp":298.77,
                        "temp_min":298.77,
                        "temp_max":298.774,
                        "pressure":1005.93,
                        "sea_level":1018.18,
                        "grnd_level":1005.93,
                        "humidity":87,
                        "temp_kf":0.26},
                    "weather":[{"id":804,"main":"Clouds","description":"overcast clouds","icon":"04d"}],
                    "clouds":{"all":88},
                    "wind":{"speed":5.71,"deg":229.501},
                    "sys":{"pod":"d"},
                    "dt_txt":"2014-07-23 09:00:00"}
                ]}
        };

        const store = mockStore({ forecast: null });

        moxios.stubRequest('https://api.openweathermap.org/data/2.5/forecast?q=Kyiv&units=metric&appid=a3563b130432ea579eb7ae78cdd2b7a1', {
            status: 200,
            response: expectedState
        })

        return store.dispatch(fetchForecast('Kyiv'))
            .then(() => {
                expect(store.getActions()).toContainEqual({ type: 'FETCH_FORECAST', payload: expectedState });
            })
    })
});