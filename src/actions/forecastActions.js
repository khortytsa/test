import {FETCH_FORECAST} from "./types";
import axios from 'axios';

export const fetchForecast = (town) => async (dispatch) => {
    await axios.get(`https://api.openweathermap.org/data/2.5/forecast?q=${town}&units=metric&appid=a3563b130432ea579eb7ae78cdd2b7a1`)
        .then(res => {
                return dispatch({
                    type: FETCH_FORECAST,
                    payload: res.data
                })
        })
        .catch((err) => console.log(err));
};