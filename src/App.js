import React, {useRef, useState} from 'react';
import Dropdown from "./Components/Dropdown";
import Item from "./Components/Item";
import { useSelector, useDispatch } from 'react-redux';
import { fetchForecast } from './actions/forecastActions';

const styles = {
  app: {
    height: '100vh'
  },
  forecastsContainer: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  forecastBlock: {
    position: 'relative',
    height: '10em',
    width: '10em',
    border: '1px solid black',
    borderRadius: '1em',
    margin: '1em',
    textAlign: 'center',
  },
  forecastWrapper: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%,-50%)'
  }
};

function App() {
  const forecast = useSelector(state => state.forecast.forecast);
  const dispatch = useDispatch();

  const [open, setOpen] = useState(false);
  const [town, setTown] = useState('');
  const appEl = useRef(null);

  const close = (e) => {
    if(e.target === appEl.current) {
      setOpen(false);
    }
  };

  const itemClick = (e) => {
    const selectedTown = e.target.innerHTML;
    setTown(selectedTown);
    dispatch(fetchForecast(selectedTown));
    setOpen(false);
  };

  const renderForecast = () => {
    if(forecast && forecast.list) {
      return (
          <div style={styles.forecastsContainer}>
            {forecast.list.map((dayForecast, i) => {
              const date = new Date(dayForecast.dt_txt);
              return (
                  <div key={`day-${i}`} style={styles.forecastBlock}>
                    <div style={styles.forecastWrapper}>
                      <div>{date.toLocaleDateString()}</div>
                      <img src={`http://openweathermap.org/img/wn/${dayForecast.weather[0].icon}@2x.png`} width="50px" height="50px" alt=""/>
                      <div>{parseInt( dayForecast.main.temp)}°C</div>
                      <div>{date.toLocaleTimeString([], {hour: '2-digit', minute: '2-digit'})}</div>
                    </div>
                  </div>
              );
            })}
          </div>
      );
    }
  };

  return (
      <div style={styles.app} className="App" ref={appEl} onClick={close}>
        <Dropdown button={<button onClick={() => setOpen(!open)}>Open Dropdown</button>}
                  open={open}>
          <Item onClick={itemClick}>Kyiv</Item>
          <Item onClick={itemClick}>Kharkiv</Item>
          <Item onClick={itemClick}>Zaporizhzhya</Item>
        </Dropdown>
        <div data-test="town">{town}</div>
        {renderForecast()}
      </div>
  );
}

export default App;
