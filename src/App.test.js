import React from 'react';
import { mount, shallow } from 'enzyme';
import App from './App';
import {Provider} from "react-redux";
import configureMockStore from "redux-mock-store";
import thunk from "redux-thunk";

const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)

describe('App Component', () => {
  let wrapper;
  beforeEach(() => {
    const store = mockStore({ forecast: {
        "city":{
          "id":1851632,
          "name":"Shuzenji",
          "coord":{
            "lon":138.933334,
            "lat":34.966671
          },
          "country":"JP",
          "timezone": 32400,
          "cod":"200",
          "message":0.0045,
          "cnt":38,
          "list":[{
            "dt":1406106000,
            "main":{
              "temp":298.77,
              "temp_min":298.77,
              "temp_max":298.774,
              "pressure":1005.93,
              "sea_level":1018.18,
              "grnd_level":1005.93,
              "humidity":87,
              "temp_kf":0.26},
            "weather":[{"id":804,"main":"Clouds","description":"overcast clouds","icon":"04d"}],
            "clouds":{"all":88},
            "wind":{"speed":5.71,"deg":229.501},
            "sys":{"pod":"d"},
            "dt_txt":"2014-07-23 09:00:00"}
          ]}
      }
    });
    wrapper = mount(
        <Provider store={store}>
          <App />
        </Provider>
        );
  });

  it('Should render Dropdown', () => {
    expect(wrapper.find(`[data-test='dropdown']`).length).toBe(1);
  });

  it('Should render Items when button clicked', () => {
    wrapper.find('button').first().simulate('click');
    expect(wrapper.find(`[data-test='item']`).length).toBe(3);
  });

  it('Should render forecast town after town selected', () => {
    wrapper.find('button').first().simulate('click');
    wrapper.find(`[data-test='item']`).first().simulate('click');
    expect(wrapper.find(`[data-test='town']`).length).toBe(1);
  });
});