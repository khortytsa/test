import React from 'react';
import PropTypes from "prop-types";

const styles = {
    container: {
        border: '1px solid black',
        cursor: 'pointer'
    }
}

const Item = ({onClick, children}) => {
    return (
        <div data-test="item" style={styles.container} onClick={onClick}>
            {children}
        </div>
    );
};

Item.propTypes = {
    onClick: PropTypes.func,
    open: PropTypes.bool,
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node
    ])
};

export default Item;