import React from "react";
import { shallow } from "enzyme";
import Dropdown from "./Dropdown";
import Item from "./Item";
import checkPropTypes from 'check-prop-types';

describe('Dropdown Component', () => {
    describe('Check PropTypes', () => {
        it('Should not through the warning', () => {
            const expectedProps = {
                button: <button data-test="button">Open Dropdown</button>,
                open: false,
                children: [<Item key="item-0"/>, <Item key="item-1"/>]
            };
            const propsError = checkPropTypes(Dropdown.propTypes, expectedProps, 'props', Dropdown.name);
            expect(propsError).toBeUndefined();
        });
    });
    describe('Has props', () => {
        let wrapper;
        beforeEach(() => {
            const props = {
                button: <button data-test="button">Open Dropdown</button>,
                open: false,
                children: [<Item key="item-0"/>, <Item key="item-1"/>]
            };
            wrapper = shallow(<Dropdown {...props}/>);
        });

        it('should render without errors', () => {
            expect(wrapper.find(`[data-test='dropdown']`).length).toBe(1);
        });

        it('should render button', () => {
            expect(wrapper.find(`button`).length).toBe(1);
        });

        it('should NOT render items when open is false', () => {
            expect(wrapper.find(<Item/>).length).toBe(0);
        });

        it('should render items when open is true', () => {
            const props = {
                button: <button>Open Dropdown</button>,
                open: true,
                children: [<Item key="item-0"/>, <Item key="item-1"/>]
            }
            wrapper = shallow(<Dropdown {...props}/>);
            expect(wrapper.find(<Item/>).length).toBe(0);
        });
    });
    describe('Has NO props', () => {
        let wrapper;
        beforeEach(() => {
            const props = {};
            wrapper = shallow(<Dropdown {...props}/>);
        });

        it('should render without errors', () => {
            expect(wrapper.find(`[data-test='dropdown']`).length).toBe(1);
        });

        it('should NOT render button', () => {
            expect(wrapper.find(`button`).length).toBe(0);
        });

        it('should NOT render items', () => {
            expect(wrapper.find(<Item/>).length).toBe(0);
        });
    });
});
