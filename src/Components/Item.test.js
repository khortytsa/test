import React from "react";
import { shallow } from "enzyme";
import Item from "./Item";

describe('Dropdown Component', () => {
    describe('Has props', () => {
        let wrapper;
        beforeEach(() => {
            const props = {
                onClick: () => {},
                children: ['Some text']
            };
            wrapper = shallow(<Item {...props}/>);
        });

        it('should render without errors', () => {
            expect(wrapper.find(`[data-test='item']`).length).toBe(1);
        });

        it('should render items text', () => {
            expect(wrapper.text()).toEqual('Some text');
        });
    });

    describe('Has NO props', () => {
        let wrapper;
        beforeEach(() => {
            const props = {};
            wrapper = shallow(<Item {...props}/>);
        });

        it('should render without errors', () => {
            expect(wrapper.find(`[data-test='item']`).length).toBe(1);
        });

        it('should render items text', () => {
            expect(wrapper.text()).toEqual('');
        });
    });
});