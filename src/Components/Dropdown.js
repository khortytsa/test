import React from 'react';
import PropTypes from 'prop-types';

const Dropdown = ({button, open, children}) => {
    return (
        <div data-test="dropdown" style={{display: 'inline-block'}}>
            {button}
            {open && children}
        </div>
    );
};

Dropdown.propTypes = {
    button: PropTypes.element,
    open: PropTypes.bool,
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node
    ])
};

export default Dropdown;