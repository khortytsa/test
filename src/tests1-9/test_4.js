// Задание 4
// Конвертнуть массив в строку.​ ​Написать тест.
// [‘apple’, ‘banan’, ‘orange’]

function stringToArray(arr) {
    if(!Array.isArray(arr)) return '';
    let result = '';
    arr.forEach((element) => {
        if (element && typeof element === 'string') {
            result += element;
        }
    });
    return result;
}

const test0 = stringToArray(['apple', 'banan', 'orange']);
console.assert(
    test0 === 'applebananorange',
    'test0'
);

const test1 = stringToArray(1);
console.assert(
    test1 === '',
    'test1'
);

const test2 = stringToArray(undefined);
console.assert(
    test2 === '',
    'test2'
);

const test3 = stringToArray(['apple', ['banan'], 'orange']);
console.assert(
    test3 === 'appleorange',
    'test3'
);
