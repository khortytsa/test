// Задание 9
// Вычислить сумму через рекурсию от числа n. Написать тест. func sumTo(100) = 100 + 99 + .... + 1 = 5050

function sumTo(n) {
    const parsedN = parseInt(n);
    if(isNaN(parsedN)) return;
    const recursiveSum = n => n > 0 ? n + recursiveSum(n - 1) : n;
    return recursiveSum(parsedN);
};

const test0 = sumTo(100);
console.assert(
    test0 === 5050,
    'test0'
);

const test1 = sumTo('100');
console.assert(
    test1 === 5050,
    'test1'
);

const test2 = sumTo(undefined);
console.assert(
    test2 === undefined,
    'test2'
);