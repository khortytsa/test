// Задание 2
// Написать функцию поиска элемента в массиве. Написать тест.
// => [ { name: ‘apple’ }, { name: ‘banan’ } ]

const someArray = [ { name: 'apple' }, { name: 'banan' } ];

const findElement = (str) => {
    if(typeof str === 'string' || str instanceof String) {
        return someArray.find(obj => obj.name === str);
    }
    return 'Please provide string';
};

const test0 = findElement('apple');
console.assert(
    typeof test0 === 'object'
    && test0.name === 'apple',
    'test0'
);

const test1 = findElement(3);
console.assert(
    typeof test1 === 'string'
    && test1 === 'Please provide string',
    'test1'
);

const test2 = findElement('orange');
console.assert(
     test2 === undefined,
    'test2'
);
