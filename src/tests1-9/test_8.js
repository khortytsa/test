// Задание 8
// Написать функцию. Поменять ключи и значения в обьекте местами. Написать тест.
// {‘key1’: ‘value1’,
// ‘key2’: ‘value2’ }

const swapKeyValue = (obj) => {
    if(!(obj instanceof Object) || Object.values(obj).some(x => !x)) return;
    return Object.keys(obj).reduce((acc, key) => {
        acc[obj[key]] = key;
        return acc
    }, {})
};

const test0 = swapKeyValue({'key1': 'value1', 'key2': 'value2'});
console.assert(
    test0 instanceof Object
    && Object.keys(test0).length === 2
    && Object.keys(test0)[0] === 'value1'
    && Object.keys(test0)[1] === 'value2'
    && Object.values(test0)[0] === 'key1'
    && Object.values(test0)[1] === 'key2',
    'test0'
);

const test1 = swapKeyValue({'key1': 'value1', 'key2': null});
console.assert(
    test1 === undefined,
    'test1'
);

const test2 = swapKeyValue({'key1': 'value1', 'key2': 'value1'});
console.assert(
    test2 instanceof Object
    && Object.keys(test2).length === 1
    && Object.keys(test2)[0] === 'value1'
    && Object.values(test2)[0] === 'key2',
    'test2'
);