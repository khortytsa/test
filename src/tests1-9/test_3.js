// Задание 3
// Найти сумму положительных элементов в массиве. Написать тест.
// => [1, 2, -10, 20, 30]

let someArray = [1, 2, -10, 20, 30];

function sumPositive() {
    if(!Array.isArray(someArray)) return 0;
    return someArray.reduce((acc, val) => {
        const n = Number(val);
        return n && n > 0 ? acc + n : acc;
    }, 0)
};

const test0 = sumPositive();
console.assert(
    test0 === 53,
    'test0'
);

someArray = [];
const test1 = sumPositive();
console.assert(
    test1 === 0,
    'test1'
);

someArray = ['1', '2', '-10', '20', '30'];
const test2 = sumPositive();
console.assert(
    test2 === 53,
    'test2'
);

someArray = ['1', '2asd', '-10', '20', '30'];
const test3 = sumPositive();
console.assert(
    test3 === 51,
    'test3'
);

someArray = ['1', null, '-10', '20', '30'];
const test4 = sumPositive();
console.assert(
    test4 === 51,
    'test4'
);

someArray = null;
const test5 = sumPositive();
console.assert(
    test5 === 0,
    'test5'
);