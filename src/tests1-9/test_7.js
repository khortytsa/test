// Задание 7
// Транспонировать матрицу.
//     [
//       [0,1,2],
//       [3,4,5],
//       [6,7,8] ]

function transponseMatrix(arr) {
    if(!arr || !Array.isArray(arr) || !arr[0] || !Array.isArray(arr[0])) return [];
    let result = new Array(arr[0].length).fill(null);
    arr.forEach((row) => {
        row.forEach((n, j) => {
            result[j] ? result[j].push(n) : result[j] = [n];
        });
    });
    return result;
}

const test0 = transponseMatrix([
    [0,1,2],
    [3,4,5],
    [6,7,8]
]);
console.assert(
    Array.isArray(test0)
    && test0.length === 3
    && test0[0][0] === 0
    && test0[0][1] === 3
    && test0[0][2] === 6

    && test0[1][0] === 1
    && test0[1][1] === 4
    && test0[1][2] === 7

    && test0[2][0] === 2
    && test0[2][1] === 5
    && test0[2][2] === 8,
    'test0'
);

const test1 = transponseMatrix([
    [0,1,2],
]);
console.assert(
    Array.isArray(test1)
    && test1.length === 3
    && test1[0][0] === 0
    && test1[1][0] === 1
    && test1[2][0] === 2,
    'test1'
);

const test2 = transponseMatrix([]);
console.assert(
    Array.isArray(test2)
    && test2.length === 0,
    'test2'
);

const test3 = transponseMatrix(undefined);
console.assert(
    Array.isArray(test3)
    && test3.length === 0,
    'test2'
);