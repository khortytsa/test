// Задание 5
// Убрать повторяющиеся элементы в массиве. Написать тест.
// [‘apple’, ‘banan’, ‘orange’, ‘orange’, ‘orange’, ‘test’]

function removeDuplicates(arr) {
    return Array.from(new Set(arr));
}

const test0 = removeDuplicates(['apple', 'banan', 'orange', 'orange', 'orange', 'test']);
console.assert(
    Array.isArray(test0)
    && test0.length === 4
    && test0.join() === ['apple', 'banan', 'orange', 'test'].join(),
    'test0'
);

const test1 = removeDuplicates(['apple', 'banan', null, 'orange', 'orange', 'test']);
console.assert(
    Array.isArray(test1)
    && test1.length === 5
    && JSON.stringify(test1) === JSON.stringify(['apple', 'banan', null, 'orange', 'test']),
    'test1'
);

const test2 = removeDuplicates([]);
console.assert(
    Array.isArray(test2)
    && test2.length === 0
    && JSON.stringify(test2) === JSON.stringify([]),
    'test2'
);

const test3 = removeDuplicates(undefined);
console.assert(
    Array.isArray(test3)
    && test3.length === 0
    && JSON.stringify(test3) === JSON.stringify([]),
    'test3'
);