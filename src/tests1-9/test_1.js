// Задание 1
// Написать функцию которая генерирует массив. Функция должна возвращать последовательно заполненный массив. Написать тест.
// => [0, 1, 2, 3]

const generateArray = len => Number(len) && len > 0 ? [...Array(len).keys()] : [];

const test0 = generateArray("empty");
console.assert(
    Array.isArray(test0)
    && test0.length === 0,
    'test0'
);

const test1 = generateArray(0);
console.assert(
    Array.isArray(test1)
    && test1.length === 0,
    'test1'
);

const test2 = generateArray(3);
console.assert(
    Array.isArray(test2)
    && test2.length === 3
    && JSON.stringify(test2) === JSON.stringify([0, 1, 2]),
    'test2'
)