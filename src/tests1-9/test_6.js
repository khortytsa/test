// Задание 6
// Найти в тексте аннограмы. Написать тест.
// Компьютеру удалось продвинуться дальше. Сначала он увеличил рекорд на одну букву: ратификация​ - ​тарификация​, а затем довел его до пятнадцати букв: ​старорежимность - ​нерасторжимость​ .
// => [ [‘ратификация’, ‘тарификация’], [‘старорежимность’, ‘нерасторжимость’] ]

class Anagram {
    constructor(sentence) {
        this.arr = sentence.match(/[\wа-я]+/ig) || [];
        this.dict = {};
    }

    get data() {
        this._fillUpDict();
        let result = [];
        Object.values(this.dict).forEach((words) => {
            if(words.length > 1) {
                result.push(words)
            }
        });
        return result;
    };

    _getKey(word) {
        return word.split('').sort().join('');
    };

    _fillUpDict() {
        this.arr.forEach(word => {
            const key = this._getKey(word);
            this.dict.hasOwnProperty(key) ? this.dict[key].push(word) : this.dict[key] = [word];
        });
    }
}

const testSentence0 = 'Компьютеру удалось продвинуться дальше. Сначала он увеличил рекорд на одну букву:' +
    ' ратификация - тарификация, а затем довел его до пятнадцати букв: старорежимность - нерасторжимость .';
const test0 = new Anagram(testSentence0).data;
console.assert(
    Array.isArray(test0)
    && test0.length === 2
    && test0[0].includes('ратификация')
    && test0[0].includes('тарификация')
    && test0[1].includes('старорежимность')
    && test0[1].includes('нерасторжимость'),
    'test0'
);

const testSentence1 = '';
const test1 = new Anagram(testSentence1).data;
console.assert(
    Array.isArray(test1)
    && test1.length === 0,
    'test1'
);
