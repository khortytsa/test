import { FETCH_FORECAST } from '../actions/types';
import forecastReducer from './forecastReducer';

describe('Forecast Reducer', () => {
    it('Should return default state', () => {
        const newState = forecastReducer(undefined, {});
        expect(newState).toEqual({ forecast: null })
    });

    it('Should return new state if receiving type', () => {
        const forecast = {
            "city":{
                "id":1851632,
                "name":"Shuzenji",
                "coord":{
                    "lon":138.933334,
                    "lat":34.966671
                },
                "country":"JP",
                "timezone": 32400,
                "cod":"200",
                "message":0.0045,
                "cnt":38,
                "list":[{
                    "dt":1406106000,
                    "main":{
                        "temp":298.77,
                        "temp_min":298.77,
                        "temp_max":298.774,
                        "pressure":1005.93,
                        "sea_level":1018.18,
                        "grnd_level":1005.93,
                        "humidity":87,
                        "temp_kf":0.26},
                    "weather":[{"id":804,"main":"Clouds","description":"overcast clouds","icon":"04d"}],
                    "clouds":{"all":88},
                    "wind":{"speed":5.71,"deg":229.501},
                    "sys":{"pod":"d"},
                    "dt_txt":"2014-07-23 09:00:00"}
                ]}
        }
            const newState = forecastReducer(undefined, {
            type: FETCH_FORECAST,
            payload: forecast
        });
        expect(newState.forecast).toEqual(forecast)
    });
});